package com.ucbcba.proyecto.proyecto.Entities;

import com.sun.istack.internal.NotNull;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "Pais")
public class Pais {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPais;
    @NotNull
    @Size(min = 1, max = 50, message = "Debe tener ser menos a 50 caracteres")
    private String name;
    @OneToMany (mappedBy = "pais")
    private Set<User> users;
    @OneToMany (mappedBy = "pais")
    private Set<Empresa> empresas;


    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Integer getidPais() {
        return idPais;
    }

    public void setidPais(Integer idPais) {
        this.idPais = idPais;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(Set<Empresa> empresas) {
        this.empresas = empresas;
    }
}
